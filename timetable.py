import json
import os
import sys
from datetime import datetime
from getpass import getpass
from pathlib import Path

from loguru import logger

from src.cli import create_settings_from_args
from src.settings import Settings
from src.google import schedule_timetable, CREDS
from src.parsing import retrieve_schedule_data, parse_schedule, Class
from src.utils import (
    parse_monday_date,
    cache_to_python,
    python_to_cache,
    date_to_json,
    PasswordBlackBox,
)
from src.webdriver import spawn

STUDENT_ID = os.environ.get("STUDENT_ID")
PASSWORD = PasswordBlackBox(os.environ.get("STUDENT_PASSWORD"))


@logger.catch()
def main(sid: int, passw: PasswordBlackBox, settings: Settings):
    schedule = None
    cache = {}
    cache_entry = None
    if settings.cache_filename.exists() and not settings.ignore_cached_schedule:
        logger.info(
            "Found the cache file. Attempting to load the most recent cache entry."
        )
        cache = json.load(settings.cache_filename.open("r"))
        entries = sorted(cache.keys(), key=lambda k: datetime.strptime(k, "%Y-%m-%d"))
        if entries:
            cache_entry = entries[-1]
            logger.info(f"Found a cache record from {cache_entry}")
            schedule = cache_to_python(cache[cache_entry])
    elif not settings.ignore_cached_schedule:
        logger.info(f"Cache file does not exist: `{settings.cache_filename}`")

    if schedule is None:
        if settings.ignore_cached_schedule:
            logger.info("Retrieving a new schedule as requested.")
        else:
            logger.info("Could not find a cached schedule, retrieving a new one.")

        with spawn(settings.driver_path, settings.headless) as driver:
            table, week = retrieve_schedule_data(driver, sid, passw)
            cookies = driver.get_cookies()
            logger.info("Successfully retrieved the schedule data.")

        monday = parse_monday_date(week)
        timetable = parse_schedule(
            table, "https://banner.centennialcollege.ca", monday, cookies
        )

        schedule = {"week": week, "monday": monday, "classes": timetable}

    if settings.no_scheduling:
        classes = schedule["classes"]
        for i, cls in enumerate(classes):
            cls: Class = cls
            if not cls.is_schedulable():
                logger.info(
                    f"Class ({i + 1} / {len(classes)}) '{cls.name}' "
                    f"has been successfully parsed, but its date appears to be invalid. "
                    f"This could happen if the class is online or hasn't been scheduled yet. "
                    f"The location for this class was `{cls.room}`.\n"
                )
                continue

            event = cls.to_event()
            logger.info(
                f"Successfully parsed a class ({i + 1} / {len(classes)}):\n"
                f"==> {event['summary']} ({cls.day.capitalize()}, from {cls.start} to {cls.end})"
            )
    else:
        schedule_timetable(schedule["classes"], settings)

    if not settings.cache_schedule:
        return

    logger.info("Writing the cache...")

    if not cache_entry:
        cache_entry = date_to_json(datetime.now())

    cache[cache_entry] = python_to_cache(schedule)
    json.dump(cache, settings.cache_filename.open("w"), indent=4)


if __name__ == "__main__":
    settings = create_settings_from_args()
    if settings.driver_path is not None:
        settings.driver_path = Path(settings.driver_path).absolute()
        if not settings.driver_path.exists():
            print(f"[ERROR] Driver path does not exist: `{settings.driver_path}`")
            exit(1)
    elif sys.platform == "win32":
        answer = input(
            "[WARNING] Your platform appears to be Windows, but you did not supply a path to your web driver.\n"
            "Are you sure that your web driver is on PATH? (the program will crash if it is not)\n"
            "Type y(es) to proceed: "
        ).strip()
        if answer not in {"y", "yes"}:
            exit(0)

    secrets = Path(settings.secrets_filename)
    if secrets.exists() and secrets.is_file():
        secrets = json.load(secrets.open("r"))
        STUDENT_ID = int(secrets.get("student_id"))
        PASSWORD.inner = secrets.get("password")
        del secrets

    if STUDENT_ID is None or PASSWORD.inner is None:
        print(
            "[INFO] StudentID or password can be omitted if you wish to use the cached schedule."
        )

    if STUDENT_ID is None:
        print(
            "[ERROR] STUDENT_ID is not set. Set the STUDENT_ID env variable, manually edit the script, or enter it now: "
        )
        STUDENT_ID = input("Student Id: ")

    if PASSWORD.inner is None:
        print(
            "[ERROR] Set the STUDENT_PASSWORD env variable, manually edit the script, or enter it now: "
        )
        PASSWORD = PasswordBlackBox(getpass())

    if not CREDS.exists() and not settings.no_scheduling:
        print(
            "[ERROR] Missing the developer credentials. You can get them here: "
            "https://developers.google.com/calendar/quickstart/python."
        )
        exit(-1)

    main(STUDENT_ID, PASSWORD, settings)

    exit(0)
