# Centennial Timetable Scheduler
A program to parse your timetable and add the classes to Google Calendar.
Please note that asynchronous classes (or any other classes without a set time and date) can be parsed but cannot be scheduled.


## Installation
1. Clone the repo.

2. If you have [poetry](https://python-poetry.org/), run `poetry install` and `poetry shell`.
Otherwise, install the requirements into a virtual environment. Examples for Windows/Linux:
   ```bash
   # Create an environment
   $ python -m venv .env
   # Activate (Linux)
   $ source env/bin/activate
   # Activate (Windows)
   $ .\.env\Scripts\activate
   # Install the requirements
   $ python -m pip install -r requirements.txt
   ```

3. Install `chromedriver` using your favorite package manager. Examples for Ubuntu/Arch Linux/Windows:
    * Ubuntu
        ```bash
        $ sudo apt-get install chromium-chromedriver
        ```
    * Arch (extra/chromium)
        ```bash
        $ sudo pacman -S chromium
        ```
    * Arch (aur/google-chrome)
        ```bash
        $ yay -S chromedriver
        ```
    * Windows
        1. Download a driver from https://chromedriver.chromium.org/downloads.
        2. Unzip the driver into a directory, record a full path to the unzipped driver.
        3. At step 5, make sure to provide the path to the program using the
           flag `--driver C:\previously\saved\path.exe`.

4. Get your Google Developer Credentials by clicking on the big blue button at https://developers.google.com/calendar/quickstart/python,
and save them to the project directory.

5. Examine the available options:
    ```bash
    (timetable)$ python timetable.py --help

    usage: timetable.py [-h] [--no-google] [--no-email-notifications] [-e T] [--no-push-notifications] [-p T] [-c P] [-i] [--no-save-cache] [-w] [--driver DRIVER]

    Add college schedule/timetable to Google Calendar

    optional arguments:
      -h, --help            show this help message and exit
      --no-google           Do not add the parsed schedule to Google Calendar. [default: False]
      --no-email-notifications
                            Disable email notifications. [default: False]
      -e T, --email-time T  Send email notifications in this much time until the start of the class (supported values: min, h, d). [default: 1d]
      --no-push-notifications
                            Disable push notifications. [default: False]
      -p T, --push-time T   Send push notifications in this much time until the start of the class (supported values: min, h, d). [default: 30min]
      -c P, --cache-file P  Path to the cache file. [default: timetable_cache.json]
      -s P, --secrets-file P
                            Path to the secrets.json file containing the student id and password. [default: secrets.json]
      -i, --ignore-cache    Ignore the cached schedule if there is any. [default: False]
      --no-save-cache       Do not cache the parsed schedule (Password and StudentID are NEVER cached). [default: False]
      -w, --windowed        Launch the driver in the windowed mode. [default: False]
      --driver DRIVER       Path to the browser driver [default: None]
    ```

6. Provide your student id and password via secrets.json file or enter them when prompted:
    ```bash
    # (a) Provide a secrets.json or (b) enter at runtime
   (timetable)$ cat secrets.json
   {
       "student_id": <id>,
       "password": <password>
   }

    # (b) Or enter at runtime
    (timetable)$ python timetable.py
    [ERROR] STUDENT_ID is not set. Set the STUDENT_ID env variable, manually edit the script, or enter it now:
    Student Id: <your centennial student id>
    [ERROR] Set the STUDENT_PASSWORD env variable, manually edit the script, or enter it now:
    Password: <your my.centennial password>
    ```

7. Run the script. You should see something like this:
    ```bash
    (timetable)$ python timetable.py

    2019-09-08 21:08:22.248 | INFO     | __main__:main:37 - Cache file does not exist: `timetable_cache.json`
    2019-09-08 21:08:22.249 | INFO     | __main__:main:43 - Could not find a cached schedule, retrieving a new one.
    2019-09-08 21:08:52.035 | INFO     | __main__:main:364 - Started the driver.
    2019-09-08 21:08:52.035 | INFO     | __main__:retrieve_schedule_data:144 - Retrieving the schedule data...
    2019-09-08 21:08:52.957 | INFO     | __main__:retrieve_schedule_data:159 - Attempting to log on...
    2019-09-08 21:08:54.780 | INFO     | __main__:retrieve_schedule_data:173 - Logged on.
    2019-09-08 21:08:54.909 | INFO     | __main__:main:370 - Successfully retrieved the schedule data.
    2019-09-08 21:08:54.909 | INFO     | __main__:main:372 - Shutting down the driver...
    2019-09-08 21:08:54.976 | INFO     | __main__:main:377 - The driver has been shut down.
    2019-09-08 21:08:54.979 | INFO     | __main__:parse_schedule:242 - Parsing the schedule...
    2019-09-08 21:08:55.011 | INFO     | __main__:parse_schedule:263 - Successfully parsed the timetable links (6 unique link[s]).
    2019-09-08 21:08:55.011 | INFO     | __main__:parse_schedule:265 - Parsing the classes at the links...
    2019-09-08 21:08:56.588 | INFO     | __main__:parse_schedule:276 - Successfully parsed the classes.
    2019-09-08 21:08:56.589 | INFO     | __main__:schedule_timetable:68 - Configuring Google API...
    2019-09-08 21:08:56.589 | INFO     | __main__:get_credentials:35 - Locating the credentials...
    2019-09-08 21:08:56.589 | INFO     | __main__:get_credentials:58 - The credentials are valid.
    2019-09-08 21:08:57.545 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (1 / 9):
    ==> PC Hardware - CNET 101 - 001 @ Progress A3-61
    ==> Link: https://www.google.com/calendar/event?eid=NDNhajRsbHFtYnYwZGF0c2lhNmVndTltZDhfMjAxOTA5MTJUMTkzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.546 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (2 / 9):
    ==> PC Hardware - CNET 101 - 001 @ Progress A3-61
    ==> Link: https://www.google.com/calendar/event?eid=NnMydWx2YXBpNDMxc3UwNGhrMjgxYmdybHNfMjAxOTA5MTNUMTYzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.546 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (3 / 9):
    ==> Math for Computer Systems I - MATH 149 - 001 @ Progress A1-11
    ==> Link: https://www.google.com/calendar/event?eid=b2U2bGs5MWJkdDVyMTV2dGxocjBnMDcwYWtfMjAxOTA5MTFUMTUzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.547 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (4 / 9):
    ==> Math for Computer Systems I - MATH 149 - 001 @ Progress L2-22
    ==> Link: https://www.google.com/calendar/event?eid=N3ZoYjE0aDl0NTNucTg1M2Q3bXBldm80bm9fMjAxOTA5MTNUMjAzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.547 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (5 / 9):
    ==> PC Operating Systems - CNET 102 - 001 @ Progress A3-21
    ==> Link: https://www.google.com/calendar/event?eid=ZGM1cjNzYXFuN21odWo5dmE2amZmbHFlaWtfMjAxOTA5MTBUMTkzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.548 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (6 / 9):
    ==> Intro to Personal Finance - GNED 127 - 004 @ Progress L1-04
    ==> Link: https://www.google.com/calendar/event?eid=bnFwbnBwYjJzdDNxaHA0dXFpdm4xdWo1Y29fMjAxOTA5MDlUMTQzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.548 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (7 / 9):
    ==> College Communications 2 (ESL) - COMM 171 - 038 @ Progress E3-17
    ==> Link: https://www.google.com/calendar/event?eid=dTNibHVyc3VyODBuc2U2bnVxcW1nNHVuOGdfMjAxOTA5MDlUMjIzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.549 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (8 / 9):
    ==> College Communications 2 (ESL) - COMM 171 - 038 @ Progress B3-01
    ==> Link: https://www.google.com/calendar/event?eid=NjQyZ29haWVqdWw5dmJrNzB1a29wc2o3MGNfMjAxOTA5MTFUMjIzMDAwWiB1c2FuZ2VvcmdAbQ
    2019-09-08 21:08:57.549 | INFO     | __main__:calendar_insertion_handler:82 - Successfully scheduled an event (9 / 9):
    ==> Introduction To Web Design - CNET 204 - 001 @ Progress D3-11
    ==> Link: https://www.google.com/calendar/event?eid=ZTZwYzMwa3U0YmdobnZtdmw5MzgzNWU4YXNfMjAxOTA5MDlUMTgzMDAwWiB1c2FuZ2VvcmdAbQ
    ```
