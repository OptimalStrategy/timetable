from datetime import datetime
from typing import Tuple, Dict, Optional

from loguru import logger


def parse_monday_date(week: str) -> datetime:
    """
    e.g. Week of Sep 02,2019
    """
    return datetime.strptime(week, "Week of %b %d,%Y")


def day_to_num(n: str) -> int:
    """
    Maps a day of the week to it's integer number.

    :param n:
    :return:
    """

    return {
        "monday": 0,
        "tuesday": 1,
        "wednesday": 2,
        "thursday": 3,
        "friday": 4,
        "saturday": 5,
        "sunday": 6,
    }[n]


def expand_day(abbr: str) -> Optional[str]:
    """
    Expands a day abbreviation to the full word.

    :param abbr: an abbreviation (M, T, W, R, F, S, U)
    :return: the full day name
    """
    return {
        "m": "monday",
        "t": "tuesday",
        "w": "wednesday",
        "r": "thursday",
        "f": "friday",
        "s": "saturday",
        "u": "sunday",
    }.get(abbr.lower())


def hours_to_delta(h: str) -> Tuple[int, int]:
    """
    Convert a string in the form `hours:minutes (am|pm)` to a tuple (digital hours, minutes).

    :param h: a time string
    :return: a tuple of (hours, minutes)
    """
    value, period = h.split(" ")
    hours, minutes = map(int, value.split(":"))

    if period == "pm" and hours < 12:
        hours += 12

    return hours, minutes


def location_to_address(room: str) -> str:
    """
    Converts a room's location to the room's campus address. Only the Progress campus is supported for now.

    :param room: a room location
    :return: the campus address
    """
    if room.startswith("Progress"):
        return "941 Progress Ave, Scarborough, ON M1G 3T8"
    elif room.strip() == "Online":
        return "Online"
    else:
        logger.warning(f"Unknown campus: {room}.")
    return room


def date_to_json(d: datetime) -> Optional[str]:
    try:
        return d.strftime("%Y-%m-%d")
    except AttributeError:
        return None


def json_to_date(s: str) -> Optional[datetime]:
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except (TypeError, ValueError):
        return None


def cache_to_python(c: dict) -> dict:
    c["monday"] = json_to_date(c["monday"])
    c["classes"] = list(map(p.Class.from_json, c["classes"]))
    return c


def python_to_cache(c: dict) -> dict:
    c["monday"] = date_to_json(c["monday"])
    c["classes"] = list(map(p.Class.to_json, c["classes"]))
    return c


class PasswordBlackBox(object):
    """
    Prevents the password from leaking to STDOUT/STDERR in case of an error.
    """

    def __init__(self, inner: str):
        self.inner = inner

    def __len__(self) -> int:
        return len(self.inner)

    def __getitem__(self, item: int):
        return self.inner[item]

    def __repr__(self):
        return f"<blackbox: password>"

    def __str__(self):
        return repr(self)


__all__ = [
    "parse_monday_date",
    "day_to_num",
    "expand_day",
    "hours_to_delta",
    "location_to_address",
    "cache_to_python",
    "python_to_cache",
    "date_to_json",
    "json_to_date",
    "PasswordBlackBox",
]

import src.parsing as p
