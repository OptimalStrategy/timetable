import os
from contextlib import contextmanager
from pathlib import Path
from typing import Optional

from loguru import logger
from selenium.webdriver import Chrome, ChromeOptions
from selenium.common import exceptions
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait


def wait(driver: Chrome, timeout: float = 3) -> WebDriverWait:
    """
    Returns :code:`WebDriverWait(driver, timeout)`

    :param timeout: the timeout in seconds
    :return: :code:`WebDriverWait` applied to the stored driver
    """
    return WebDriverWait(driver, timeout)


def just_wait(driver: Chrome, timeout: float = 3):
    """
    Waits the given number of seconds.

    :param driver: an instance of the driver
    :param timeout: the number of seconds to wait,
    :return: None
    """
    try:
        WebDriverWait(driver, timeout).until(lambda _: False)
    except exceptions.TimeoutException:
        pass


def by_id(
    driver: Chrome, id_: str, timeout: float = 0.5, index: int = 0
) -> Optional[WebElement]:
    """
    Attempts to the element with the id :code:`id_`.
    Returns None the element does not exist or the timeout is exceeded.

    :param id_: the id to retrieve
    :param timeout: the timeout
    :return: the found element or None
    """
    try:
        return wait(driver, timeout).until(lambda x: x.find_elements_by_id(id_)[index])
    except (exceptions.NoSuchElementException, exceptions.TimeoutException, IndexError):
        return None


def by_class(
    driver: Chrome, cls: str, timeout: float = 0.5, index: int = 0
) -> Optional[WebElement]:
    """
    Attempts to an element with the class :code:`cls`.
    Returns None the element does not exist or the timeout is exceeded.

    :param class: the class to find
    :param timeout: the timeout
    :return: the found element or None
    """
    try:
        return wait(driver, timeout).until(
            lambda x: x.find_elements_by_class_name(cls)[index]
        )
    except (exceptions.NoSuchElementException, exceptions.TimeoutException, IndexError):
        return None


@contextmanager
def spawn(executable: Optional[Path] = None, headless: bool = True) -> Chrome:
    """
    Spawns a configurably headless webdriver using the given executable. Automatically shutdowns the driver.

    :param executable: chromedriver executable
    :return: a Chrome instance
    """
    opts = ChromeOptions()

    if headless:
        opts.add_argument("--headless")

    kw = {"options": opts}
    if executable:
        epath = executable.absolute().resolve()
        kw["executable_path"] = str(epath)
        # Make sure that the driver is on PATH.
        if epath.exists() and str(epath.parent) not in os.environ["PATH"]:
            os.environ["PATH"] += os.pathsep + str(epath.parent)

    driver = Chrome(**kw)

    logger.info("Started the driver.")

    try:
        yield driver
    finally:
        logger.info("Shutting down the driver...")

        driver.close()
        driver.quit()

        logger.info("The driver has been shut down.")
