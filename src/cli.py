from argparse import (
    ArgumentParser,
    HelpFormatter,
    SUPPRESS,
    OPTIONAL,
    ZERO_OR_MORE,
    ArgumentTypeError,
)
from pathlib import Path

from src.settings import Settings, TIME_PATTERN


class AddDefaultsToHelpFormatter(HelpFormatter):
    def _get_help_string(self, action):
        help = action.help
        if "%(default)" not in action.help:
            if action.default is not SUPPRESS:
                defaulting_nargs = [OPTIONAL, ZERO_OR_MORE]
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += " [default: %(default)s]"
        return help


def validate_time(s: str):
    t = TIME_PATTERN.match(s)
    if t is None or t.group(1) == "0":
        raise ArgumentTypeError(
            f"Time patterns are expected to be positive, non-zero integers ending with `min`, `h`, or `d`. Got `{s}`."
        )
    return s


def get_parser(default_cache_file: Path, default_secrets_file: Path) -> ArgumentParser:
    parser = ArgumentParser(
        prog="timetable.py",
        description="Add college schedule/timetable to Google Calendar",
        formatter_class=AddDefaultsToHelpFormatter,
    )
    parser.add_argument(
        "--no-google",
        action="store_true",
        default=False,
        help="Do not add the parsed schedule to Google Calendar.",
    )
    parser.add_argument(
        "--no-email-notifications",
        action="store_true",
        default=False,
        help="Disable email notifications.",
    )
    parser.add_argument(
        "-e",
        "--email-time",
        type=validate_time,
        default="1d",
        metavar="T",
        help="Send email notifications in this much time until the start of the class (supported values: min, h, d).",
    )
    parser.add_argument(
        "--no-push-notifications",
        action="store_true",
        default=False,
        help="Disable push notifications.",
    )
    parser.add_argument(
        "-p",
        "--push-time",
        type=validate_time,
        default="30min",
        metavar="T",
        help="Send push notifications in this much time until the start of the class (supported values: min, h, d).",
    )
    parser.add_argument(
        "-c",
        "--cache-file",
        type=str,
        default=str(default_cache_file),
        metavar="P",
        help="Path to the cache file.",
    )
    parser.add_argument(
        "-s",
        "--secrets-file",
        type=str,
        default=str(default_secrets_file),
        metavar="P",
        help="Path to the secrets.json file containing the student id and password.",
    )
    parser.add_argument(
        "-i",
        "--ignore-cache",
        action="store_true",
        default=False,
        help="Ignore the cached schedule if there is any.",
    )
    parser.add_argument(
        "--no-save-cache",
        action="store_true",
        default=False,
        help="Do not cache the parsed schedule (Password and StudentID are NEVER cached).",
    )
    parser.add_argument(
        "-w",
        "--windowed",
        action="store_true",
        default=False,
        help="Launch the driver in the windowed mode.",
    )
    parser.add_argument(
        "--driver", type=str, default=None, help="Path to the browser driver"
    )
    return parser


def create_settings_from_args() -> "Settings":
    from timetable import Settings

    args = get_parser(
        Settings().cache_filename, Settings().secrets_filename
    ).parse_args()
    s = Settings(
        secrets_filename=Path(args.secrets_file).absolute(),
        cache_filename=Path(args.cache_file).absolute(),
        cache_schedule=not args.no_save_cache,
        ignore_cached_schedule=args.ignore_cache,
        push_notifications=not args.no_push_notifications,
        push_notifications_time=args.push_time,
        email_notifications=not args.no_email_notifications,
        email_notifications_time=args.email_time,
        no_scheduling=args.no_google,
        driver_path=args.driver,
        headless=not args.windowed,
    )
    return s
