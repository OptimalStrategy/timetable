from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import List, Dict, Any, Tuple, Optional

import requests
from loguru import logger
from bs4 import BeautifulSoup as BS4

from src.utils import *
from src.webdriver import by_id, Chrome, exceptions, by_class


#: The url of the timetable.
URL = "https://banner.centennialcollege.ca/ssomanager/c/SSB?pkg=bwskfshd.P_CrseSchd?start_date_in="


class InvalidDateError(Exception):
    pass


@dataclass
class Class:
    link: str
    name: str
    crn: str
    start: str
    end: str
    room: str
    day: Optional[str]
    date: datetime
    end_date: datetime

    def to_event(
        self, email_time: int = 24 * 60, push_time: int = 30
    ) -> Dict[str, Any]:
        """
        Transforms the class to a Google Calendar API event request.

        :param self: a Class instance
        :return: request data
        """
        if self.day is None:
            raise InvalidDateError(self)

        n_weeks = (self.end_date - self.date).days // 7

        hours, minutes = hours_to_delta(self.start)
        start_time = datetime(
            year=self.date.year,
            month=self.date.month,
            day=self.date.day,
            hour=hours,
            minute=minutes,
        )

        hours, minutes = hours_to_delta(self.end)
        end_time = datetime(
            year=self.date.year,
            month=self.date.month,
            day=self.date.day,
            hour=hours,
            minute=minutes,
        )

        fmt = lambda d: d.strftime("%Y-%m-%dT%H:%M:00.0")

        overrides = []
        if email_time:
            overrides.append({"method": "email", "minutes": email_time})
        if push_time:
            overrides.append({"method": "popup", "minutes": push_time})

        event = {
            "summary": f"{self.name} @ {self.room}",
            "location": location_to_address(self.room),
            "description": f"Class: {self.name}\nRoom: {self.room}\nCRN: {self.crn}\nLink: {self.link}",
            "start": {"dateTime": fmt(start_time), "timeZone": "America/Toronto"},
            "end": {"dateTime": fmt(end_time), "timeZone": "America/Toronto"},
            "recurrence": [f"RRULE:FREQ=WEEKLY;COUNT={n_weeks}"],
            "reminders": {"useDefault": False, "overrides": overrides},
        }
        return event

    def to_json(self) -> dict:
        return {
            "link": self.link,
            "name": self.name,
            "crn": self.crn,
            "start": self.start,
            "end": self.end,
            "room": self.room,
            "day": self.day,
            "date": date_to_json(self.date),
            "end_date": date_to_json(self.end_date),
        }

    @classmethod
    def from_json(cls, json: dict) -> "Class":
        json["date"] = json_to_date(json["date"])
        json["end_date"] = json_to_date(json["end_date"])
        return cls(**json)

    def is_schedulable(self) -> bool:
        return (
            self.day is not None
            and self.start not in ("TBA", None)
            and self.end not in (None, "TBA")
            and self.date is not None
        )


def retrieve_schedule_data(
    driver: Chrome, sid: int, passw: PasswordBlackBox
) -> Tuple[str, str]:
    """
    Opens the timetable in the given browser. Logs on and retrieves the table's html.

    :param driver: a Chrome instance
    :param sid: student id
    :param passw: student password
    :return: a tuple of (table_code, week name)
    """
    logger.info("Retrieving the schedule data...")

    driver.get(URL)

    while driver.current_url.endswith("authenticationendpoint/retry.do"):
        logger.warning(
            "Authentication stuck at retry.do. Trying to refresh the page every 5 seconds."
        )

        driver.refresh()

    logger.info("Attempting to log on...")

    username = by_id(driver, "username")
    username.send_keys(sid)

    password = by_id(driver, "password")
    password.send_keys(passw)

    submit = by_id(driver, "mysubmit1")
    try:
        submit.click()
    except exceptions.ElementNotInteractableException:
        pass

    logger.info("Logged on.")

    table = by_class(driver, "datadisplaytable")
    page = table.get_attribute("innerHTML")
    week = by_class(driver, "fieldlargetext").text
    return page, week


def parse_schedule(
    table: str, base_url: str, monday_date: datetime, cookies: List[Dict[str, Any]]
) -> List[Class]:
    """
    Extracts the links to individual classes from the timetable and parses them.
    Returns a list of all classes in the timetable.

    :param table: table html
    :param base_url: base timetable url to format the links
    :param monday_date: the week's monday date
    :param cookies: cookies from the driver
    :return: a list of all classes in the timetable
    """
    logger.info("Parsing the schedule...")

    agenda = set()
    schedule = []

    tree = BS4(table, "html.parser")
    root = tree.find_all("tr")
    header, rows = root[0], root[1:]

    # Parse classes' links
    for row in rows:
        columns = row.find_all("td")
        for i, col in enumerate(columns):
            if "ddlabel" not in col.get("class"):
                continue

            a = col.find("a")
            link = "{}{}".format(base_url, a.get("href"))
            agenda.add(link)

    logger.info(
        f"Successfully parsed the timetable links ({len(agenda)} unique link[s])."
    )
    logger.info("Parsing the classes at the links...")

    # Visit each url and parse the schedule
    with requests.Session() as s:
        for cookie in cookies:
            s.cookies.set(cookie["name"], cookie["value"])

        for url in agenda:
            r = s.get(url)
            schedule.extend(_parse_schedule(url, r.content, monday_date))

    logger.info("Successfully parsed the classes.")

    return schedule


def _parse_schedule(url: str, page: bytes, monday: datetime) -> List[Class]:
    """
    Parses an course page.

    :param url: a link to a course page
    :param page: the page itself
    :param monday: the week's monday
    :return: a list of classes in the course
    """
    tree = BS4(page, "html.parser")
    course_info, schedule = tree.find_all("table", attrs={"class": "datadisplaytable"})

    name = course_info.find("caption").text
    crn = course_info.find_all("tr")[1].text.split(":")[1].strip()

    classes = []
    for row in schedule.find_all("tr")[1:]:  # Skip the header
        ty, time, day, location, date_range, schedule_ty = map(
            lambda x: x.text, row.find_all("td")[:-1]
        )  # omit the instructors column
        day = expand_day(day)
        end_date = datetime.strptime(date_range.split("-")[-1].strip(), "%b %d,%Y")

        if time.strip() == "TBA":
            start = "TBA"
            end = "TBA"
            date = None
        else:
            start, end = map(str.strip, time.split("-"))
            hours, minutes = hours_to_delta(start)
            date = monday + timedelta(
                days=day_to_num(day), hours=hours, minutes=minutes
            )

        classes.append(
            Class(
                link=url,
                name=name,
                crn=crn,
                start=start,
                end=end,
                room=location,
                day=day,
                date=date,
                end_date=end_date,
            )
        )

    return classes
