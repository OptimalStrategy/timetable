import pickle
from pathlib import Path
from typing import List

from loguru import logger

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import BatchHttpRequest


# If modifying these scopes, delete the file token.pickle.
from src.parsing import Class
from src.settings import Settings

SCOPES = ["https://www.googleapis.com/auth/calendar"]
PROJECT_DIR = Path(__file__).absolute().resolve().parent.parent
TOKEN = PROJECT_DIR.joinpath("token.pickle")
CREDS = PROJECT_DIR.joinpath("credentials.json")


def get_credentials():
    """
    Requests and saves Google API credentials.

    :return: credentials
    """
    logger.info("Locating the credentials...")

    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if TOKEN.exists():
        with TOKEN.open("rb") as token:
            creds = pickle.load(token)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        logger.warning("Missing the credentials. Please log in.")

        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CREDS, SCOPES)
            creds = flow.run_local_server(port=0)

        # Save the credentials for the next run
        with TOKEN.open("wb") as token:
            pickle.dump(creds, token)
    else:
        logger.info("The credentials are valid.")

    return creds


def schedule_timetable(classes: List["Class"], settings: Settings):
    """
    Adds the given list of classes to the calendar.
    """

    logger.info("Configuring Google API...")

    creds = get_credentials()
    service = build("calendar", "v3", credentials=creds)
    batch = service.new_batch_http_request()

    state = {"n_scheduled": 0, "n_events": len(classes)}

    def calendar_insertion_handler(_, response, exception):
        if exception is not None:
            logger.error(f"Couldn't insert an event: {exception}")
        else:
            state["n_scheduled"] += 1
            logger.info(
                f"Successfully scheduled an event ({state['n_scheduled']} / {state['n_events']}):\n"
                f"==> {response['summary']}\n"
                f"==> Link: {response['htmlLink']}"
            )

    for cls in classes:
        if not cls.is_schedulable():
            logger.info(
                f"The date for '{cls.name}' appears to be invalid. "
                f"This could happen if the class is online or hasn't been scheduled yet. "
                f"The location for this class was `{cls.room}`.\nWe can't schedule this class."
            )
            continue
        batch.add(
            service.events().insert(
                calendarId="primary",
                body=cls.to_event(
                    settings.email_time_in_minutes(), settings.push_time_in_minutes()
                ),
            ),
            calendar_insertion_handler,
        )

    batch.execute()
