import re
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

TIME_PATTERN = re.compile(r"([0-9]+)(min|h|d)")


@dataclass
class Settings:
    secrets_filename: Path = "secrets.json"
    cache_filename: Path = "timetable_cache.json"
    cache_schedule: bool = True
    ignore_cached_schedule: bool = False
    push_notifications: bool = True
    push_notifications_time: str = "30min"
    email_notifications: bool = True
    email_notifications_time: str = "1d"
    no_scheduling: bool = False
    driver_path: Optional[Path] = None
    headless: bool = True

    def push_time_in_minutes(self) -> int:
        if not self.push_notifications:
            return 0
        m = TIME_PATTERN.match(self.push_notifications_time)
        base, time = m.groups()
        return self.time_to_minutes(int(base), time)

    def email_time_in_minutes(self) -> int:
        if not self.email_notifications:
            return 0
        m = TIME_PATTERN.match(self.email_notifications_time)
        base, time = m.groups()
        return self.time_to_minutes(int(base), time)

    @staticmethod
    def time_to_minutes(base: int, time: str) -> int:
        if time == "d":
            minutes = base * 24 * 60
        elif time == "h":
            minutes = base * 60
        else:
            minutes = base
        return minutes
